/**
 * Разработчик Сергей Новоселецкий
 * @type Array
 */

var collection = [];
var timer1;
var timerCount = 0;
var args = {};
var containerview;

var class_supSlider = {
    init: function (tempArgs) {
        /**
         * Пояснения:
         * timeOut - Кол-во сек до следующего слайда
         * nav - Навигационные точки (true - включены | false - выключены)
         * nav_arrow - Стрелки по бокам (true - включены | false - выключены)
         * height - Высота всего блока
         * start - с какой картинки начнём (по порядку 0..1..2..3..)
         * stopOnHover - При наведении на картинку, заморозить таймер автоматической смены изображения
         */
        
        args = $.extend({"timeOut": "4000", "nav": true, "height": "auto", 
                         "start": 0, "stopOnHover": false}, tempArgs);
        
        containerview = this;
        
        $(this).find("img").each(function (i, t) {
            var href_data = $(t).attr("src");
            collection[i] = href_data;
            $(this).hide();
        });

        class_supSlider.slide(args.start);

        if (args.nav !== null && args.nav === true)
            class_supSlider.nav(collection.length);

        class_supSlider.navArrow();  

        if (args.stopOnHover === true) {
            $(this).hover(function () {
                clearInterval(timer1);
            }, function () {
                class_supSlider.startTimer(args.timeOut);
            });
        }
    },
    startTimer: function (timeOut) {
        if (collection.length > 1) {
            timer1 = setInterval(function () {

                ++timerCount;

                if (timerCount >= collection.length) {
                    timerCount = 0;

                    $(".nav-points ul li").removeClass("active");
                }

                class_supSlider.slide(timerCount);
            }, timeOut);
        } else {
            return false;
        }

    },
    
    slide: function (item) {
        var col;

        clearInterval(timer1);

        if (typeof (item) === "number")
            col = item;
        else
            col = item.item;

        if (!collection[col]) {
            col = 0;
        }

        $(containerview).find("img").removeClass("active");
        $(".nav-points ul li").removeClass("active");
        $(".nav-points ul li").eq(col).addClass("active");
        $(containerview).find("img[src='" + collection[col] + "']").addClass("active");
        
        $(containerview).stop().fadeOut(400, function () {
            $(this).stop().fadeIn(400).css({
                height: args.height === "auto" ? $(containerview).find("img[src='"+collection[col]+"']").height() : args.height,
                backgroundImage: "url('" + collection[col] + "')"
            });
            timerCount = col;

            if (args.timeOut !== false)
                class_supSlider.startTimer(args.timeOut);
        });
    },

    
    nav: function (count_items) {
        $(containerview).append("<nav class='nav-points'><ul></ul></nav>");

        for (var i = 0; i < count_items; i++) {
            $(containerview).find("nav.nav-points ul").append("<li><a href='javascript:;' onclick='$("+containerview+").supSlider(\"slide\", {\"item\":" + i + "});'>" + i + "</a></li>");
        }

        $(".nav-points ul li:first-child").addClass("active");
    },
    
    navArrow: function () {
        $("a.arrow-right").on('click', function () {
            var col = $(containerview).find("img.active").attr("src");
            var item = class_supSlider.findImgToCollction(col);
            class_supSlider.slide(item + 1);
            return false;
        });
        
        $("a.arrow-left").on('click', function () {
            var col = $(containerview).find("img.active").prev().attr("src");
            var item = class_supSlider.findImgToCollction(col);
            class_supSlider.slide(item - 1);
            return false;
        });
    },
    
    findImgToCollction: function (path) {
        for (var i = 0; i < collection.length; i++) {
            if (collection[i] === path) {
                return i;
            }
        }
    }
};

$.fn.supSlider = function (object) {
    if (class_supSlider[object] !== null && typeof (object) === "string") {
        class_supSlider[object].apply(this, Array.prototype.slice.call(arguments, 1));
    }
};